import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:iesbgran/app/constants.dart';
import 'package:iesbgran/app/modules/register/register_store.dart';
import 'package:mobx/mobx.dart';

class RegisterPage extends StatefulWidget {
  final String title;
  const RegisterPage({Key? key, this.title = 'Cadastre-se'}) : super(key: key);

  @override
  RegisterPageState createState() => RegisterPageState();
}

class RegisterPageState extends ModularState<RegisterPage, RegisterStore> {

  late PageController _pageController;

  late final TextEditingController _nameController;
  late final TextEditingController _emailController;
  late final TextEditingController _passwordController;

  late final ReactionDisposer _disposer;

  @override
  void initState(){
    super.initState();
    _pageController = PageController(initialPage : 0);

    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    when(
            (_) => store.user != null,
            () => Modular.to.pushReplacementNamed(Cosntants.Routes.HOME)
    );
  }

  @override
  void dispose(){
    _disposer();
    return super.dispose();
  }

  late final Widget _form = PageView(
    controller: _pageController,
    scrollDirection: Axis.vertical,
    physics: NeverScrollableScrollPhysics(),
    children: [
      _FormField(
        controller: _nameController,
        label: 'Informe seu nome',
        showBackButton: false,
        onNext: (){
          _pageController.nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        },
        onBack: (){
          _pageController.previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        },
      ),
      _FormField(
        controller: _emailController,
        label: 'Informe o seu e-mail',
        showBackButton: true,
        onNext: (){
          _pageController.nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        },
        onBack: (){
          _pageController.previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        },
      ),
      _FormField(
        controller: _passwordController,
        label: 'Informe sua senha',
        showBackButton: true,
        onNext: (){
          // _pageController.nextPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);

          store.registerUser(
              name: _nameController.text,
              email: _emailController.text,
              password: _passwordController.text
          );
        },
        onBack: (){
          _pageController.previousPage(duration: Duration(seconds: 1), curve: Curves.easeInOut);
        },
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
            TextButton(
              child: Text('Login', style: TextStyle(color: Theme.of(context).buttonColor)),
              onPressed: () {
                Modular.to.pushNamed(Cosntants.Routes.LOGIN);
              },
            )
          ]
      ),
      body: Observer(
        builder: (_){
          if(store.loading){
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                Text('Aguarde... Salvando seu cadastro....')
              ],
            );
          }
          return _form;
        },
      )
    );
  }
}

class _FormField extends StatelessWidget{

  final bool showBackButton;
  final String label;
  final TextEditingController controller;
  final VoidCallback onNext;
  final VoidCallback onBack;

  _FormField({required this.controller, this.showBackButton = true, required this.label, required this.onNext, required this.onBack });

  @override
  Widget build(BuildContext context) {
    return Expanded(

        child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
              showBackButton
                 ? IconButton(icon: Icon(Icons.arrow_upward), onPressed: onBack, iconSize: 40)
                 : IconButton(icon: Icon(Icons.arrow_downward), onPressed: onNext, iconSize: 40),
              Spacer(),
              Flexible(
                child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 1),
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                        FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline1!.copyWith(fontSize: 40),
                              maxLines: 1)
                        ),
                        TextFormField(
                          controller: controller,
                          onEditingComplete: onNext,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                          ),
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        )
                      ],
                    )
                )
            )
        ],
    ));
  }

}