import 'package:flutter_modular/flutter_modular.dart';
import 'package:iesbgran/app/modules/feed/feed_module.dart';
import 'package:iesbgran/app/modules/profile/profile_module.dart';
import 'package:iesbgran/app/modules/search/search_module.dart';
import 'package:iesbgran/app/constants.dart';

import 'home_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(
        Modular.initialRoute,
        child: (_, args) => HomePage(),
        children: [
          ModuleRoute(Cosntants.Routes.FEED, module: FeedModule(), transition: TransitionType.fadeIn),
          ModuleRoute(Cosntants.Routes.SEARCH, module: SearchModule(), transition: TransitionType.fadeIn),
          ModuleRoute(Cosntants.Routes.PROFILE, module: ProfileModule(), transition: TransitionType.fadeIn),
        ]
    ),
  ];
}