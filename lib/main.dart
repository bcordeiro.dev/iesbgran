import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/app_module.dart';
import 'app/app_widget.dart';

// void main() => runApp(ModularApp(module: AppModule(), child: AppWidget()));
void main() async {
  // Flutter não inicializado aqui.
  WidgetsFlutterBinding.ensureInitialized();
  // Flutter já iniciado.

  final sharedPreferences = await SharedPreferences.getInstance();
  final fireBaseApp = await Firebase.initializeApp();
  return runApp(ModularApp(module: AppModule(sharedPreferences, fireBaseApp), child: AppWidget()));
}